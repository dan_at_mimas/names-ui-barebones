package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

public class FieldsOfActivity extends Controller {
  
    public static Result list() {
        return ok(fields_of_activity.render());
    }

}